# O3K --- A simple x86 Kernel
---------------------------


## Installation Requirements (Linux x86_64):
--

1. gcc (with x86)
2. nasm/yasm (build script is using yasm, can be changed easily)
3. meson/ninja (build system)


## Build Instructions:

1. meson build
2. cd build
3. ninja


## How to Run:
---

### QEMU:

Needed software:
  1. qemu
  2. qemu-arch-extra (for 32bit support)

qemu-system-i386 -kernel kernel


### copy.sh/v86

[Virtual x86](https://copy.sh/v86/) is an online x86 emulator
In order to boot the kernel we have to create a bootable grub image

Software needed: 
1. grub
2. mtools
3. xorriso

```bash
cd build
mkdir -p img/boot/grub
cp kernel img/boot/kernel.bin
echo "menuentry \"o3k\" {      \
	multiboot /boot/myos.kernel \
}		" >> img/boot/grub/grub.cfg
grub-mkrescue -o o3k.iso img
```
This iso file can be imported as a CD Rom




