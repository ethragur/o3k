#include "common.h"
#include "../video/tty.h"
#include <stdarg.h>

void kmemset(void * ptr, int c, size_t n)
{
   unsigned char *p = ptr;
   for(size_t i = 0; i < n; i++)
   {
      *p = (unsigned char)c;
      p++;
   }
}

/*
 * Careful, could leave dest not terminated with '\0'
 */
char * kstrncpy( char * dest, const char * src, size_t n)
{
   size_t i;

   for(i = 0; i < n && src[i] != '\0'; i++)
   {
      dest[i] = src[i];
   }
   for(; i < n; i++)
   {
      dest[i] = '\0';
   }

   return dest;
}

size_t kstrlen(const char * str)
{
   size_t len = 0;

   for(; *str != '\0';str++, len++){};

   return len + 1;
}

void * strrev(char * str)
{
   if(str)
   {
      char * end = str + kstrlen(str) - 2;

#define XOR_SWAP(a,b) do\
      {\
         a ^= b;\
         b ^= a;\
         a ^= b;\
      } while (0)

      while(str < end)
      {
         XOR_SWAP(*str, *end);
         str++;
         end--;
      }
   }

   return str;
}


/* ***************************************
 * Convert number into string,
 * Only works for bases <= 10 
 * cause ASCII stanadard sucks:
 * shouldve been '1234567890ABCDEFG...' 
 * Null Terminated
 * ***************************************/
char * itoa( int value, char * str, int base )
{
    char * rc;
    char * ptr;
    // Check for supported base.
    if ( base < 2 || base > 36 )
    {
        *str = '\0';
        return str;
    }
    rc = ptr = str;
    // Set '-' for negative decimals.
    if ( value < 0 && base == 10 )
    {
        *ptr++ = '-';
    }
    // The actual conversion.
    do
    {
        // Modulo is negative for negative value. This trick makes abs() unnecessary.
        *ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz"[35 + value % base];
        value /= base;
    } while ( value );
    // Terminating the string.
    *ptr-- = '\0';
    return strrev(rc);
}


/* ***************************************
 * 
 * ***************************************/
char * ksnprintf(char * c, size_t n, const char * format, ...)
{
   size_t i = 0;
   const char * reader;

   va_list arg;
   va_start(arg, format);

   for(reader = format; *reader != '\0' && i < n; reader++)
   {
      if( *reader != '%' && *reader != '\0')
      {
         c[i++] = *reader;
      }
      else
      {
         char x;
         int32_t d;
         char *s;
         char dec[32];
         reader++; //jump over '%'

         switch(*reader)
         {
            case 'c':   
               x = va_arg(arg, int32_t);
               c[i++] = x;
               break;
            case 's':   
               s  = va_arg(arg, char *);
               for(size_t j = 0; s[j] != '\0' && i < n; i++, j++)
               {
                  c[i] = s[j];
               }
               break;
            case 'd':
               d = va_arg(arg, int32_t);
               itoa(d,dec, 10);
               for(size_t j = 0; dec[j] != '\0' && i < n; i++, j++)
               {
                  c[i] = dec[j];
               }
               break;
            case 'x':
               d = va_arg(arg, int32_t);
               itoa(d,dec, 16);
               for(size_t j = 0; dec[j] != '\0' && i < n; i++, j++)
               {
                  c[i] = dec[j];
               }
               break;
            default:
               break;
         }
      }
   }

   for(; i < n; i++)
   {
      c[i] = '\0';
   }

   va_end(arg);


   return c;
}

// Write a byte out to the specified port.
void outb(uint16_t port, uint8_t value)
{
   asm volatile ("outb %1, %0" : : "dN" (port), "a" (value));
}

uint8_t inb(uint16_t port)
{
   uint8_t ret;
   asm volatile("inb %1, %0" : "=a" (ret) : "dN" (port));
   return ret;
}

uint16_t inw(uint16_t port)
{
   uint16_t ret;
   asm volatile ("inw %1, %0" : "=a" (ret) : "dN" (port));
   return ret;
} 

