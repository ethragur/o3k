#ifndef COMMON_H
#define COMMON_H

#include <stdint.h>
#include <stddef.h>

void outb(uint16_t port, uint8_t value);
uint8_t inb(uint16_t port);
uint16_t inw(uint16_t port);
void kmemset(void *ptr, int c, size_t n);
char * ksnprintf(char * c, size_t n, const char * format, ...);
size_t kstrlen(const char * str);

#endif
