#include "bitset.h"

/* *****************************************
 *	returns the index for the field
 * *****************************************/
static inline uint32_t bm_get_index(uint32_t field)
{
	return field / (sizeof(uint32_t) * BYTE);
}

/* *****************************************
 * returns the offset from the field	
 * *****************************************/
static inline uint32_t bm_get_offset(uint32_t field)
{
	return field % (sizeof(uint32_t) * BYTE);
}

void bm_set_field(bitset_t * bitset, uint32_t field)
{
	uint32_t index  = bm_get_index(field/PAGE_ALIGN);
	uint32_t offset = bm_get_offset(field/PAGE_ALIGN);

	bitset->fields[index] |= (0x1 << offset); 
}

void bm_clear_field(bitset_t * bitset, uint32_t field)
{
	uint32_t index  = bm_get_index(field/PAGE_ALIGN);
	uint32_t offset = bm_get_offset(field/PAGE_ALIGN);

	bitset->fields[index] &= ~(0x1 << offset); 
}

uint32_t bm_test_field(bitset_t * bitset, uint32_t field)
{
	uint32_t index  = bm_get_index(field/PAGE_ALIGN);
	uint32_t offset = bm_get_offset(field/PAGE_ALIGN);

	return (bitset->fields[index] & (0x1 << offset)); 
}

uint32_t bm_get_next(bitset_t * bitset)
{
	for(uint32_t i = 0; i < bm_get_index(bitset->size); i++)
	{
		//no space available
		if(bitset->fields[i] != 0xFFFFFFFF)
		{
			for(uint32_t j = 0; j < sizeof(uint32_t) * BYTE; j++)
			{
				uint32_t toTest = 0x1 << j;
				if( !(bitset->fields[i] & toTest) )
				{
					return i*sizeof(uint32_t)*BYTE+j;
				}
			}
		}
	}
}

