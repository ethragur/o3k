#pragma once


#include <stdint.h>
#include <stddef.h>

//looks better
#define BYTE 8
//we are only interested in 4KB values
#define PAGE_ALIGN 0x1000

struct _bitset_t
{
	uint32_t  *fields;
	uint32_t  size;
} typedef bitset_t;

/* *****************************************
 * Sets the field inside the set to 1
 * *****************************************/
void bm_set_field(bitset_t * bitset, uint32_t field);

/* *****************************************
 * Sets the field inside the set to 0
 * *****************************************/
void bm_clear_field(bitset_t * bitset, uint32_t field);

/* *****************************************
 * Returns the value of the field
 * *****************************************/
uint32_t bm_test_field(bitset_t * bitset, uint32_t field);

/* *****************************************
 * Returns the next free field
 * *****************************************/
uint32_t bm_get_next(bitset_t * bitset);
