#include "libs/common.h"
#include "video/tty.h"
#include "arch/i386/descriptor_tables.h"
#include "arch/i386/timer.h"
#include "input/keyboard.h"




void kmain(void)
{
   init_descriptor_tables();  
   tty_init();

   asm volatile("sti");

   init_timer(1);
   init_keyboard();

   for(;;);
}
