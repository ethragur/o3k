#include "tty.h"


#define WIDTH 80
#define HEIGHT 25
/* Times 2 because of color value */
#define SCREENSIZE 2 * WIDTH * HEIGHT

/* current cursor location */
static size_t tty_row;
static size_t tty_col;
static uint8_t tty_color;
static volatile uint16_t * tty_buf;

/* video memory begins at address 0xb8000 */
static volatile uint16_t * vidptr = (uint16_t*)0xb8000;


/* The VGA Color Entries */
enum vga_color 
{
   VGA_COLOR_BLACK = 0,
   VGA_COLOR_BLUE = 1,
   VGA_COLOR_GREEN = 2,
   VGA_COLOR_CYAN = 3,
   VGA_COLOR_RED = 4,
   VGA_COLOR_MAGENTA = 5,
   VGA_COLOR_BROWN = 6,
   VGA_COLOR_LIGHT_GREY = 7,
   VGA_COLOR_DARK_GREY = 8,
   VGA_COLOR_LIGHT_BLUE = 9,
   VGA_COLOR_LIGHT_GREEN = 10,
   VGA_COLOR_LIGHT_CYAN = 11,
   VGA_COLOR_LIGHT_RED = 12,
   VGA_COLOR_LIGHT_MAGENTA = 13,
   VGA_COLOR_LIGHT_BROWN = 14,
   VGA_COLOR_WHITE = 15,
};

/* Returns a Foreground Background Color Mix */
static inline uint8_t vga_entry_color(enum vga_color fg, enum vga_color bg) {
	return fg | bg << 4;
}
 
/* return an entry for the VGA Buffer */
static inline uint16_t vga_entry(unsigned char uc, uint8_t color) {
	return (uint16_t) uc | (uint16_t) color << 8;
}

/* *****************************************
 * Initialize the screen by setting all the 
 * VGA values, 
 * Can also be used to clear the screen
 * *****************************************/
void tty_init()
{
   tty_row   = 0;
   tty_col   = 0;
   tty_color = vga_entry_color( VGA_COLOR_LIGHT_GREEN, VGA_COLOR_BLACK );
   tty_buf   = vidptr;

   for(size_t x = 0; x < WIDTH; x++)
   {
      for(size_t y = 0; y < HEIGHT; y++)
      {
         tty_buf[y * WIDTH + x] = vga_entry(' ', tty_color);
      }
   }
}   

void tty_putat(const unsigned char c, uint8_t color, size_t x, size_t y)
{
   tty_buf[y * WIDTH + x] = vga_entry(c, color);
}

void tty_putc(const char c)
{
   tty_putat((unsigned char) c, tty_color, tty_col, tty_row);
   tty_col = (tty_col == WIDTH) ? 0 : tty_col+1;
   if( tty_col == 0)
   {
      tty_row = (tty_row == HEIGHT) ? 0 : tty_row+1;
      ++tty_col;
   }
}

void tty_puts(const char * str)
{
   for(; *str != '\0'; str++)
   {
      tty_putc(*str);
   }
}
