#pragma once

#include <stdint.h>
#include <stddef.h>


void tty_init();
void tty_putat(const unsigned char c, uint8_t color, size_t x, size_t y);
void tty_putc(const char c);
void tty_puts(const char * str);
