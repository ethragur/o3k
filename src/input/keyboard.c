
#include "keyboard.h"
#include "../libs/common.h"
#include "../arch/i386/isr.h"
#include "../video/tty.h"
#include "keyboard_map.h"

void keyboard_handler(registers_t *regs)
{
   unsigned char scancode;
   scancode = inb(0x60);
   
   if(scancode & 0x80)
   {
      //shift alt or control keys...
   }
   else if(!(scancode & 0x0080))
   {
      tty_putc(keyboard_map[scancode]);
   }
}

void init_keyboard()
{
   register_interrupt_handler(33, &keyboard_handler);
}



